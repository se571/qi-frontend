import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';
import { ViewStudentComponent } from './view-student/view-student.component';
import { WeatherComponent } from './weather/weather.component';
import { EventComponent } from './event/event.component';
import { MUmapComponent } from './mumap/mumap.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'mainpage', component: MainpageComponent},
  {path: 'footer', component: FooterComponent},
  {path: 'header', component: HeaderComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'view-student', component: ViewStudentComponent},
  {path: 'weather', component: WeatherComponent},
  {path: 'event', component: EventComponent},
  {path: 'mumap', component: MUmapComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
