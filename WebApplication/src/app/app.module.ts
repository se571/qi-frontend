import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';
import { ViewStudentComponent } from './view-student/view-student.component';
import { WeatherComponent } from './weather/weather.component';
import { EventComponent } from './event/event.component';
import { PayComponent } from './pay/pay.component';
import { PaymeComponent } from './payme/payme.component';
import { CookieService } from 'ngx-cookie-service';
import { MUmapComponent } from './mumap/mumap.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainpageComponent,
    FooterComponent,
    HeaderComponent,
    RegisterComponent,
    ViewStudentComponent,
    WeatherComponent,
    EventComponent,
    PayComponent,
    PaymeComponent,
    MUmapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
  ],
  providers: [
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
