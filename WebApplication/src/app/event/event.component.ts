import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.less']
})
export class EventComponent implements OnInit {
  private condition = false;
  constructor(private cookieService: CookieService) { }

  ngOnInit() {
    this.condition = this.cookieService.check('loginCheck');
  }

}
