import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  passwordVisible = false;
  private cookieValue: string;
  private isVisible = false;

  constructor(private fb: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private cookieService: CookieService
) { }

  ngOnInit() {
    this.cookieService.deleteAll();
  }
  login(f: NgForm) {
    this.http.post('http://SE571.test/login',
      {
        email: f.controls['email'].value,
        password: f.controls['password'].value,
      })
      .subscribe(data => {
        if (data === true) {
          this.router.navigate(['/mainpage']);
          this.cookieService.set('loginCheck', 'testCookieValue');
          this.cookieValue = this.cookieService.get('loginCheck');

        } else {
          alert("Wrong Username or Password !")
        }
      });
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }


}
