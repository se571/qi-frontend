import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MUmapComponent } from './mumap.component';

describe('MUmapComponent', () => {
  let component: MUmapComponent;
  let fixture: ComponentFixture<MUmapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MUmapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MUmapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
