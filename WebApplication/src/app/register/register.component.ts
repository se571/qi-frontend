import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientModule, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
    private condition = false;
  private isVisible = false;
  private isVisibleError = false;
  private isVisibleAll = false;
  list;
  formModel: FormGroup;

  constructor(private http: HttpClient, private jsonp: HttpClientJsonpModule, fb: FormBuilder, private cookieService: CookieService) {
  this.formModel = fb.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    studentid: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', Validators.required],
    country: [''],
    department: [''],
  });
  }
  postData(f: NgForm) {

    if(this.formModel.valid) {
      this.http.post('http://SE571.test/user/new',
        {
          firstname: f.controls['firstname'].value,
          lastname: f.controls['lastname'].value,
          studentid: f.controls['studentid'].value,
          email: f.controls['email'].value,
          password: f.controls['password'].value,
          country: f.controls['country'].value,
          department: f.controls['department'].value,
        })
        .subscribe(
          data => {
            console.log('POST Request is successful', data);
            this.isVisible = true;
          },
          error => {
            console.log('Error', error);
            this.isVisibleError = true;
          });
    } else {
      this.isVisibleAll = true;
    }
  }
  ngOnInit() {
    this.condition = this.cookieService.check('loginCheck');
  }

}
