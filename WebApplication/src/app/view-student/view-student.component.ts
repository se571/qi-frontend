import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-view-student',
  templateUrl: './view-student.component.html',
  styleUrls: ['./view-student.component.less']
})
export class ViewStudentComponent implements OnInit {
  private condition = false;
  isOkLoading = false;
  private firstname: any;
  private lastname: any;
  private studentid: any;
  private email: any;
  private password: any;
  private country: any;
  private department: any;
  formModel: FormGroup;
  list;
  private headers = new Headers({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient,
    private jsonp: HttpClientJsonpModule,
    fb: FormBuilder,
    private cookieService: CookieService
  ) {
  this.formModel = fb.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    studentid: ['', Validators.required],
    email: ['', [ Validators.required]],
    password: ['', Validators.required],
    country: [''],
    department: [''],
  });
}

 // Request data
 getData() {
  this.http.get('http://SE571.test/users').subscribe((data)=>{
    console.log(data);
    this.list = data;
  });
}

delete(id){
  this.http.get('http://SE571.test/user/delete/' + id).subscribe((data)=>{
    console.log(data);
    location.reload();
  });
}


  ngOnInit() {
    this.getData();
    this.condition = this.cookieService.check('loginCheck');
  }

}
